
def isValid(my_cal, friend_required_calorie, fruit_cal_dic,fruit_juice_in_cupboard, key):
    if my_cal + fruit_cal_dic[key] > friend_required_calorie:
        return False
    if fruit_juice_in_cupboard.count(key) == 0:
        return False
    return True

def find_juice(number_of_fruits, my_cal, friend_required_calorie, fruit_cal_dic,fruit_juice_in_cupboard, key):

    if my_cal == friend_required_calorie:
        return True
    if key == chr(ord('a') + number_of_fruits) :
        return False

    for i in range(fruit_juice_in_cupboard.count(key)):
        if isValid(my_cal, friend_required_calorie, fruit_cal_dic, fruit_juice_in_cupboard, key):

            my_cal = my_cal + fruit_cal_dic[key]
            fruit_juice_in_cupboard = fruit_juice_in_cupboard.replace(key,'', 1)
            result['result']=result['result']+key
            if find_juice(number_of_fruits, my_cal, friend_required_calorie, fruit_cal_dic,fruit_juice_in_cupboard, chr(ord(key) + i)) == True:
                return True

            my_cal = my_cal - fruit_cal_dic[key]
            fruit_juice_in_cupboard = fruit_juice_in_cupboard + key
            result['result'] = result['result'].replace(key,'', 1)

    return False


if __name__ == "__main__":
    number_of_friends = int(input())
    for i in range(0, number_of_friends):
        l = list(map(int, input().split()))
        number_of_fruits = l[0]
        calories = l[1:]
        fruit_cal_dic = {chr(ord('a') + i): calories[i] for i in range(0, number_of_fruits)}
        fruit_juice = input()
        friend_required_calorie = int(input())

        my_cal = 0
        result = {'result':''}
        fruit_juice_in_cupboard = fruit_juice
        if find_juice(number_of_fruits, my_cal, friend_required_calorie, fruit_cal_dic,fruit_juice_in_cupboard, 'a') == False:
            print("SORRY, YOU JUST HAVE WATER")
        else:
            print(result['result'])

